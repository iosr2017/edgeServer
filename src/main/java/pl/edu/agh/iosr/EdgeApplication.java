package pl.edu.agh.iosr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.filters.route.ZuulFallbackProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@SpringBootApplication
@EnableZuulProxy
@RefreshScope
public class EdgeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdgeApplication.class, args);
	}


	@Bean
	public ZuulFallbackProvider steamFallbackProvider() {
		return new ZuulFallbackProvider() {
			@Override
			public String getRoute() {
				return "aggregate-service";
			}

			@Override
			public ClientHttpResponse fallbackResponse() {
				return getClientHttpResponse("[\n" +
                        "    {\n" +
                        "        \"url\": \"http://lubimyczytac.pl/ksiazka/4635992/spider-man-niebieski\",\n" +
                        "        \"title\": \"Spider-Man: Niebieski\",\n" +
                        "        \"author\": {\n" +
                        "            \"url\": \"http://lubimyczytac.pl/autor/24488/jeph-loeb\",\n" +
                        "            \"fullName\": \"Jeph Loeb\"\n" +
                        "        },\n" +
                        "        \"cover\": \"http://s.lubimyczytac.pl/upload/books/4635000/4635992/572306-50x75.jpg\",\n" +
                        "        \"category\": \"book\",\n" +
                        "        \"rating\": 7.7155172413793,\n" +
                        "        \"publisher\": \"Imperial Entertainment. Imperial Entertainment,\",\n" +
                        "        \"year\": \"2012\",\n" +
                        "        \"available\": true\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"url\": null,\n" +
                        "        \"title\": \"Dzień dobry, Marcin. Miękkie lądowanie. Miękkie lądowanie Marcina\",\n" +
                        "        \"author\": null,\n" +
                        "        \"cover\": null,\n" +
                        "        \"category\": null,\n" +
                        "        \"rating\": null,\n" +
                        "        \"publisher\": \"S.D.T.-Film. S.D.T. Film,\",\n" +
                        "        \"year\": \"2012\",\n" +
                        "        \"available\": true\n" +
                        "    },\n" +
                        "]");
			}
		};
	}


	private ClientHttpResponse getClientHttpResponse(String response) {
		return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.OK;
            }

            @Override
            public int getRawStatusCode() throws IOException {
                return 200;
            }

            @Override
            public String getStatusText() throws IOException {
                return "OK";
            }

            @Override
            public void close() {

            }

            @Override
            public InputStream getBody() throws IOException {
                return new ByteArrayInputStream(response.getBytes());
            }

            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                return headers;
            }
        };
	}
}
